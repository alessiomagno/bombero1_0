#Bombero, sistema di prevenzione e alerting di incendi boschivi

Lo scopo del progetto è quello di realizzare un real-time autonomous system per far fronte al pericolo di incendi boschivi che interessa maggiormente i terreni nel periodo estivo. 
Si gestirà a fondo il problema del loro monitoraggio poiché spesso isolati e distanti dal centro abitato.

#Funzioni del sistema:

Prevenzione: rilevazione, tramite una rete di sensori, delle grandezze fisiche significative per il calcolo del rischio.
Alerting: triangolazione della posizione della zona ad alto rischio di infiammabilità e comunicazione della situazione di pericolo a caserme VV.FF. e al proprietario del terreno.
Intervento: umidificazione della zona a rischio in attesa dell’arrivo dei VV.FF. al fine di rallentare la combustione.

L’implementazione seguirà un modello ad agenti coordinato attraverso l’utilizzo di un situated tuple center.