package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic Carbon monoxide sensor.
 *
 */
public interface ICOSensor extends ISensor{
	
	/**
	 * @return the concentration of CO in the air.
	 */
	double getConcentration();

}
