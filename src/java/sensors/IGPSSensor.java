package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic GPS module.
 *
 */
public interface IGPSSensor extends ISensor{

	/**
	 * @return the latitude of the position.
	 */
	double getLat();
	/**
	 * @return the longitude of the position.
	 */
	double getLong();
}
