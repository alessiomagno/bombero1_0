package sensors;

import java.util.Random;

import utils.Strings;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Class to simulate&test humidity sensor with random generation of logic values.
 */
public class MockHumiditySensor implements IHumiditySensor {

	@Override
	public double getHumidity() {
		return Utils.round((new Random().nextDouble() * 100) % 100, 2);
	}

	@Override
	public String getName() {
		return Strings.MOCK_HUMIDITY_SENSOR;
	}

}
