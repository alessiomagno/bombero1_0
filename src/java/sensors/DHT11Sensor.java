package sensors;

import utils.Strings;

/**
 * 
 * @author Alessio Addimandos
 * One possible use of a temperature sensor and a humidity sensor using DHT11
 */

public class DHT11Sensor implements ITemperatureSensor,IHumiditySensor{

	private final int defaultPin = 3;
	private MultiPurposeSensorDHT11 sensor = MultiPurposeSensorDHT11.getInstance(defaultPin);
	
	
	@Override
	public String getName() {
		return Strings.DHT11_SENSOR;
	}

	@Override
	public double getHumidity() {
		return sensor.getHumidity();
	}

	@Override
	public double getTemp() {
		return sensor.getTemperature();
	}

}
