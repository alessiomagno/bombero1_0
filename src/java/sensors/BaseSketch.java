package sensors;

import com.pi4j.io.gpio.GpioController;
import com.pi4j.io.gpio.GpioPin;
import com.pi4j.io.gpio.PinMode;
import com.pi4j.io.gpio.PinPullResistance;
import com.pi4j.io.gpio.PinState;
import com.pi4j.wiringpi.Gpio;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Alessio Addimando
 *
 *  An abstract and general implementation for a BaseSketch configuring WiringPi for the use of a particular sensor with Raspberry.
 * @param <X> the type returned from a sensor that represents its dimension target.
 */

public abstract class BaseSketch<X>{

   protected static Thread threadCheckInputStream;
   protected boolean isNotInterrupted = true;
   private GpioController gpio;

   protected void setup() {
       if (Gpio.wiringPiSetup() == -1) {
           System.out.println("GPIO SETUP FAILED!");
           return;
       }
       System.out.println("GPIO SETUP SUCCESSFUL!");
   }
   
   protected abstract X getState();
   
   public BaseSketch(GpioController gpio) {
       this.gpio = gpio;
       this.setup();
   }

   protected static void wiringPiSetup(){
       if (Gpio.wiringPiSetup() == -1) {
           String msg = "==>> GPIO SETUP FAILED";
           System.out.println(msg);
           throw new RuntimeException(msg);
       }
   }
   
   protected void tearDown() {
       System.out.println("Shutting down gpio.");
       if(gpio != null){
           for(GpioPin pin : gpio.getProvisionedPins()){
               if(pin.getMode().equals(PinMode.DIGITAL_OUTPUT)){
                   pin.setShutdownOptions(true, PinState.LOW, PinPullResistance.OFF);
               }
           }                
           gpio.shutdown();
       }
   }

   protected void delayMilliseconds(long miliseconds) {
       try {
           TimeUnit.MILLISECONDS.sleep(miliseconds);
       } catch (InterruptedException ex) {
    	   System.out.println("Sleep Milliseconds delay interrupted " + ex.getMessage());
       }
   }
   
   protected void delayMicroseconds(long microseconds){
       try {
           TimeUnit.MICROSECONDS.sleep(microseconds);
       } catch (InterruptedException ex) {
    	   System.out.println("Sleep Microseconds delay interrupted " + ex.getMessage());
       }
   }

 

}