package sensors;

import java.util.Random;

import utils.Strings;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Class to simulate&test an anemometer with random generation of logic values.
 */
public class MockWindSensor implements IWindSensor {

	@Override
	public double getWindSpeed() {
		return Utils.round((new Random().nextDouble() * 100) % 70, 2);
	}

	@Override
	public String getName() {
		return Strings.MOCK_WIND_SENSOR;
	}

}
