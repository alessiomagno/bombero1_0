package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic sensor
 */
public interface ISensor {
/**
 * 
 * @return the conventional name of the sensor
 */
  String getName();
}
