package sensors;
/**
 * 
 * @author Alessio Addimando
 * An implementation of a DHT11 sensor connector. 
 */
import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.wiringpi.Gpio;

public class MultiPurposeSensorDHT11 extends BaseSketch<Object> {
    private static final int    MAXTIMINGS  = 85;
    private final int[]         dht11_dat   = { 0, 0, 0, 0, 0 };
    private final int pin;
    private static MultiPurposeSensorDHT11 instance =null;
    private double temperature=19;
    private double humidity=25;

    private MultiPurposeSensorDHT11(int pin) {
    	super(GpioFactory.getInstance());
    	this.pin = pin;
    
    }
    
    public synchronized static MultiPurposeSensorDHT11 getInstance(int pin)
    {
      if (instance == null)
      {
    	  instance = new MultiPurposeSensorDHT11(pin);
      }

      return instance; 
    }

   @Override
    protected Object getState() {
    	return null;
    }

    private void getData() {
        int laststate = Gpio.HIGH;
        int j = 0;
        dht11_dat[0] = dht11_dat[1] = dht11_dat[2] = dht11_dat[3] = dht11_dat[4] = 0;

        Gpio.pinMode(this.pin, Gpio.OUTPUT);
        Gpio.digitalWrite(pin, Gpio.LOW);
        Gpio.delay(18);

        Gpio.digitalWrite(pin, Gpio.HIGH);
        Gpio.pinMode(pin, Gpio.INPUT);

        for (int i = 0; i < MAXTIMINGS; i++) {
            int counter = 0;
            while (Gpio.digitalRead(pin) == laststate) {
                counter++;
                Gpio.delayMicroseconds(1);
                if (counter == 255) {
                    break;
                }
            }

            laststate = Gpio.digitalRead(this.pin);

            if (counter == 255) {
                break;
            }

            /* ignore first 3 transitions */
            if (i >= 4 && i % 2 == 0) {
                /* shove each bit into the storage bytes */
                dht11_dat[j / 8] <<= 1;
                if (counter > 16) {
                    dht11_dat[j / 8] |= 1;
                }
                j++;
            }
        }
        // check we read 40 bits (8bit x 5 ) + verify checksum in the last
        // byte
        if (j >= 40 && checkParity()) {
            float h = (float) ((dht11_dat[0] << 8) + dht11_dat[1]) / 10;
            if (h > 100) {
                h = dht11_dat[0]; // humidity
            }
            float c = (float) (((dht11_dat[2] & 0x7F) << 8) + dht11_dat[3]) / 10;
            if (c > 125) {
                c = dht11_dat[2]; // temperature
            }
            if ((dht11_dat[2] & 0x80) != 0) {
                c = -c;
            }
            this.humidity=h;
            this.temperature=c;
            
        } else {
        	System.out.println("Data not available, problem with sensor");
        }

    }

    private boolean checkParity() {
        return dht11_dat[4] == (dht11_dat[0] + dht11_dat[1] + dht11_dat[2] + dht11_dat[3] & 0xFF);
    }
    
    
    public double getHumidity() {
    	this.getData();
    	return this.humidity;
    	
    }

    public double getTemperature() {
    	this.getData();
    	return this.temperature;
    	
    }

/**
 * Main to test sensor DHT11
 */
public static void main(final String ars[]){

	final int testPin = 7;
    final MultiPurposeSensorDHT11 dht =  MultiPurposeSensorDHT11.getInstance(testPin);

    while(true) {
        try {
			Thread.sleep(2000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
        try
        { 
        	dht.getData();
            System.out.println("Temperature: "+dht.getTemperature()+" Humidity: "+dht.getHumidity());
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
    } 

}
}