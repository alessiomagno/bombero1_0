package sensors;

import java.util.Random;

import utils.Strings;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Class to simulate&test COSensor with random generation of logic value.
 */
public class MockCOSensor implements ICOSensor {

	@Override
	public double getConcentration() {
		return Utils.round((new Random().nextDouble() * 300) % 200, 2);
	}

	@Override
	public String getName() {
		return Strings.MOCK_CO_SENSOR;
	}

}
