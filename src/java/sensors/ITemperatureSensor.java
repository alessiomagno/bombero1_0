package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic temperature sensor.
 *
 */
public interface ITemperatureSensor extends ISensor{
	/**
	 * 
	 * @return the temperature value of that area.
	 */
	double getTemp();
}
