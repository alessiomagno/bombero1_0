package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic humidity sensor.
 *
 */
public interface IHumiditySensor extends ISensor{
	/**
	 * @return the value of the humidity percentage
	 */
	double getHumidity();
}
