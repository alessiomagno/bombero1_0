package sensors;

import utils.Strings;
import utils.enumerations.MQ2State;

/**
 * 
 * @author Alessio Addimandos
 * One possible use of a temperature sensor and a humidity sensor using DHT11
 */

public class MQ2Sensor implements ICOSensor{

	private final int defaultPin = 7;
	private MQ2Connector sensor = MQ2Connector.getIstance(defaultPin);
	
	@Override
	public String getName() {
		return Strings.MQ2_SENSOR;
	}

	@Override
	public double getConcentration() {
		if(sensor.getState().equals(MQ2State.LOW))
		{
			return 101;
		}
		return 0;
	}

}
