package sensors;

import java.util.Random;

import utils.Strings;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Class to simulate&test GpsSensor with defaults values.
 */
public class MockGpsSensor implements IGPSSensor {

	Random r;
	
	public MockGpsSensor() {
		r=new Random(System.nanoTime());
	}

	@Override
	public String getName() {
		return Strings.MOCK_GPS_SENSOR;
	}

	@Override
	public double getLat() {
		return Utils.round(32.141700+((r.nextDouble()%100)/100),6);
	}

	@Override
	public double getLong() {
		return Utils.round(-110.860800+((r.nextDouble()%100)/100),6);
	}

}
