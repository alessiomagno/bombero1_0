package sensors;
/**
 * 
 * @author Alessio Addimando
 * Interface for a generic anemometer.
 *
 */
public interface IWindSensor extends ISensor{
	/**
	 * @return the speed of the wind in that area.
	 */
	double getWindSpeed();

}
