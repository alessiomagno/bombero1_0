package sensors;

import com.pi4j.io.gpio.GpioFactory;
import com.pi4j.wiringpi.Gpio;

import utils.enumerations.MQ2State;
/**
 * 
 * @author Alessio Addimando
 * An implementation of a MQ2 sensor connector. 
 */
public class MQ2Connector extends BaseSketch<MQ2State>{
	

		private static MQ2Connector instance;
		private int pin;

		public MQ2Connector(int pin){
	        super(GpioFactory.getInstance());
	        this.pin = pin;
	        
	    }
	    
		public static synchronized MQ2Connector getIstance(int pin){
			if(instance == null){
				instance = new MQ2Connector(pin);
			}
			return instance;
		}
		/**
		 * @return the state of the digital value (depending on threshold)
		 */
		@Override
		protected MQ2State getState(){
			if(Gpio.digitalRead(pin) == 1) return MQ2State.HIGH;
			return MQ2State.LOW;
				
			
		}
/**
* Main to test sensor MQ2Connector.
*/			
		public static void main(String[] args) throws InterruptedException {
				final int testPin = 7;
				MQ2Connector mq2 = MQ2Connector.getIstance(testPin);
				while(true){
					System.out.println(mq2.getState());
				}
		}	  
}
	
