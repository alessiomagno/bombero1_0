package sensors;

import java.util.Random;

import utils.Strings;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Class to simulate&test temperature sensor with random generation of logic values.
 */
public class MockTemperatureSensor implements ITemperatureSensor {

	@Override
	public double getTemp() {
		return Utils.round((new Random().nextDouble() * 10000) % 100, 2);
	}

	@Override
	public String getName() {
		return Strings.MOCK_TEMPERATURE_SENSOR;
	}

}
