package utils;
/**
 * A class representing a triplet of values
 * @author Edoardo Antonini
 *
 * @param <T> the first element generic type
 * @param <U> the second element generic type
 * @param <V> the third element generic type
 */
public class Triplet<T, U, V> {

    private T first;
    private U second;
    private V third;

    public Triplet(T first, U second, V third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    public T getFirst() { return first; }
    public U getSecond() { return second; }
    public V getThird() { return third; }
    
	@Override
	public String toString() {
		return "Triplet [first=" + first + ", second=" + second + ", third=" + third + "]";
	}
    
    
}
