package utils;

import alice.tucson.service.TucsonNodeService;
import jason.asSemantics.DefaultInternalAction;
/**
 * This class creates and instance a Tucson ndoe
 * @author Alessio Addimando
 *
 */
public class InstanceTucsonNode extends DefaultInternalAction {
	
	private static final long serialVersionUID = -8884006723998285576L;
	private static TucsonNodeService node;


	public synchronized static TucsonNodeService getIstance(int port){
		if(node == null)
		{
			node = new TucsonNodeService(port);
			node.install();
		}
		return node;
	}
}