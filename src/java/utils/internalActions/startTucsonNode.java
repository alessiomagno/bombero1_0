package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.Term;
import utils.InstanceTucsonNode;
/**
 * Action that starts the TuCSoN node
 * @author Edoardo Antonini
 *
 */
public class startTucsonNode extends DefaultInternalAction{
	
   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {

	   InstanceTucsonNode.getIstance(Integer.parseInt(args[1].toString().replace("\"", "")));
       return 1;
   }
}
