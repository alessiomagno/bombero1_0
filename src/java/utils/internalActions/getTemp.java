package utils.internalActions;

import com.google.gson.JsonObject;

import env.ForestDataRetriver;
import env.IForestDataRetriver;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import utils.Strings;
/**
 * Gets the temperature data
 * @author Edoardo Antonini
 *
 */
public class getTemp extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;
   private final IForestDataRetriver dataRetriver = ForestDataRetriver.getInstance();

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {

		JsonObject json = this.dataRetriver.getSensorData();
		Double temp = json.getAsJsonObject(Strings.DHT11_SENSOR).get(Strings.VALUE_1).getAsDouble();
       return un.unifies(args[0], new NumberTermImpl(temp));
   }
}
