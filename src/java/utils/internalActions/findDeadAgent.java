package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ObjectTermImpl;
import jason.asSyntax.Term;
/**
 * Finds the agent that is dead by reading the error message
 * @author Edoardo Antonini
 *
 */
public class findDeadAgent  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String errorMessage = args[0].toString().replace(" ", "");
	   int firstIndex = errorMessage.indexOf("MTS-error")+31;
	   String temp=errorMessage.substring(firstIndex);
	   int secondIndex = temp.indexOf("@");
	   return un.unifies(args[1], new ObjectTermImpl(errorMessage.substring(firstIndex,firstIndex+secondIndex)));
   }
}
