// Internal action code for project bombero

package utils.internalActions;

import jason.asSemantics.*;
import jason.asSyntax.*;
import webInterface.WWWService;
/**
 * Action that starts the web service for publishing data
 * @author Edoardo Antonini
 *
 */
public class startWebService extends DefaultInternalAction {

	private static final long serialVersionUID = 263851739965232013L;

	@Override
    public Object execute(TransitionSystem ts, Unifier un, Term[] args) throws Exception {
    	try {
			new WWWService().start(WWWService.getValidPortParam());
		} catch (Exception e) {
			System.out.println("Startup Error"+ e);
		}
        return true;
    }
}
