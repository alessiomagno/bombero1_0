package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
/**
 * Class that converts string to double
 * @author Edoardo Antonini
 *
 */
public class string2double extends DefaultInternalAction{

	private static final long serialVersionUID = -3642174613831889257L;

	@Override
	   public Object execute(final TransitionSystem ts, final Unifier un,
	           final Term[] args) throws Exception {
		   String data = args[0].toString().substring(1, args[0].toString().length()-1);
	       return un.unifies(args[1], new NumberTermImpl(Double.parseDouble(data)));
	}
}
