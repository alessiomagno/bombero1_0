package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
/**
 * Class that converts string to int
 * @author Edoardo Antonini
 *
 */
public class string2int extends DefaultInternalAction{

	private static final long serialVersionUID = -6764672317037804839L;

	@Override
	   public Object execute(final TransitionSystem ts, final Unifier un,
	           final Term[] args) throws Exception {
		   String data = args[0].toString().substring(1, args[0].toString().length()-1);
	       return un.unifies(args[1], new NumberTermImpl(Integer.parseInt(data)));   
	}
}
