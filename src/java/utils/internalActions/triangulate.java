package utils.internalActions;


import java.util.ArrayList;
import java.util.List;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import utils.MagicNumbers;
import utils.Pair;
import utils.Triplet;
import utils.Utils;
import webInterface.BomberoDashboard;
/**
 * Action that compute the possible position of a fire based on supervisors GPS data & CO values
 * @author Edoardo Antonini
 *
 */
public class triangulate extends DefaultInternalAction{

	private static final long serialVersionUID = -3232817225864692819L;
	private boolean emergency = false;

	@Override
	   public Object execute(final TransitionSystem ts, final Unifier un,
	           final Term[] args) throws Exception {
		   double localCo = Double.parseDouble(args[0].toString());
		   double localLat = Double.parseDouble(args[1].toString());
		   double localLong = Double.parseDouble(args[2].toString());
		   Pair<Double,Double> finalPosition = null;
		   if(args[3].toString().equals("[]"))
		   {
			   if(localCo > MagicNumbers.GAS_THRESOLD)
			   {
				   finalPosition = new Pair<Double, Double>(localLat, localLong);
				   emergency = true;
				   BomberoDashboard.setAlarm(emergency);
				   BomberoDashboard.setGPSCoordinate(finalPosition.getFirst()+","+finalPosition.getSecond());
			   }else
			   {
				   emergency = false;
				   BomberoDashboard.setAlarm(false);
			   }
			   return un.unifies(args[6], new NumberTermImpl((emergency ? 1 : 0)));
		   }
		   String[] centerData = args[3].toString()
				   							.replace("c", "")
				   							.replace("(", "")
				   							.replace(")", "")
				   							.replace("\"", "")
				   							.replace("[", "")
				   							.replace("]", "")
				   							.split(",");
		   List<Triplet<Double,Double,Double>> centerDataList = new ArrayList<>();
		   List<Triplet<Integer,Double,Double>> outOfThreshold;
		   List<Triplet<Double,Double,Double>> cartesianLocation;

		   Triplet<Double,Double,Double> triplet;
		   if(centerData.length>0)
		   {
			   for(int i=0;i<centerData.length;i++)
			   {
				   if(i%3 == 0)
				   {
					   triplet=new Triplet<>(Double.parseDouble(centerData[i])
							   				,Double.parseDouble(centerData[i+1])
							   				,Double.parseDouble(centerData[i+2]));
					   centerDataList.add(triplet);
				   }
			   }
			   centerDataList.add(new Triplet<Double, Double, Double>(localCo, localLat, localLong));
			   
			   outOfThreshold = prepareOutOfThresholdList(centerDataList);
			   if(outOfThreshold.size() > 0)
			   {
				   cartesianLocation = prepareCartesianList(outOfThreshold);
				   finalPosition = computeFirePosition(outOfThreshold,
						   								cartesianLocation,
						   								computeTotalWeight(outOfThreshold));
				   System.out.println(finalPosition.toString());
				   emergency = true;
				   BomberoDashboard.setAlarm(emergency);
				   BomberoDashboard.setGPSCoordinate(finalPosition.getFirst()+","+finalPosition.getSecond());
			   }else
			   {
				   emergency = false;
				   BomberoDashboard.setAlarm(false);
			   }
		   }
		   if(finalPosition != null)
		   {
			   un.unifies(args[4], new StringTermImpl(finalPosition.getFirst().toString()));
			   un.unifies(args[5], new StringTermImpl(finalPosition.getSecond().toString()));
		   }
		   return un.unifies(args[6], new NumberTermImpl((emergency ? 1 : 0)));
	   }
	
	
	private Pair<Double, Double> computeFirePosition(List<Triplet<Integer,Double,Double>> outOfThreshold,
													List<Triplet<Double,Double,Double>> cartesianLocation,
													double totWeight)
	{
		
		double avgX=0,avgY=0,avgZ=0;
		for(Triplet<Double, Double, Double> t: cartesianLocation)
		{
			double weight = outOfThreshold.get(cartesianLocation.indexOf(t)).getFirst();
			avgX += t.getFirst()*weight;
			avgY += t.getSecond()*weight;
			avgZ += t.getThird()*weight;
		}
		avgX/=totWeight;
		avgY/=totWeight;
		avgZ/=totWeight;
		
		double avgLat = Math.atan2(avgY, avgX);
		double hyp = Math.sqrt(avgX*avgX+avgY*avgY);
		double avgLong = Math.atan2(avgZ, hyp);
		
		avgLat *= 180/Math.PI;
		avgLong *= 180/Math.PI;
		
		return new Pair<Double, Double>(Utils.round(avgLong, 7), Utils.round(avgLat, 7));
	}
	
	private List<Triplet<Integer, Double, Double>> prepareOutOfThresholdList(List<Triplet<Double,Double,Double>> centerDataList)
	{
		List<Triplet<Integer,Double,Double>> outOfThreshold = new ArrayList<>();
		for(Triplet<Double,Double,Double> t: centerDataList)
		{
			if(t.getFirst() > MagicNumbers.GAS_THRESOLD)
			{
				Triplet<Integer,Double,Double> toAdd = new Triplet<>((int)(t.getFirst()-MagicNumbers.GAS_THRESOLD),
																	t.getSecond(),
																	t.getThird());
				outOfThreshold.add(toAdd);
			}
		}
		outOfThreshold.sort((a,b) -> (int) (b.getFirst() - a.getFirst())); //descending order
		outOfThreshold = outOfThreshold.subList(0, Math.min(outOfThreshold.size(),3));//only the max 3
		return outOfThreshold;
	}
	
	private List<Triplet<Double,Double,Double>> prepareCartesianList(List<Triplet<Integer,Double,Double>> outOfThreshold)
	{
		List<Triplet<Double,Double,Double>> cartesianLocation = new ArrayList<>();
		for(Triplet<Integer,Double,Double> t : outOfThreshold)
		{
			double cartesianSecond = t.getSecond()*Math.PI/180;
			double cartesianThird = t.getThird()*Math.PI/180;
			double x = Math.cos(cartesianSecond)*Math.cos(cartesianThird);
			double y = Math.cos(cartesianSecond)*Math.sin(cartesianThird);
			double z = Math.sin(cartesianSecond);
			cartesianLocation.add(new Triplet<Double, Double, Double>(x, y, z));
		}
		
		return cartesianLocation;
	}
	
	private double computeTotalWeight(List<Triplet<Integer,Double,Double>> outOfThreshold)
	{
		double totWeight = 0;
		for(Triplet<Integer,Double,Double> t : outOfThreshold)
		{
			totWeight+=t.getFirst();
		}
		return totWeight;
	}
}
