package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import utils.Strings;
/**
 * Derives the ip from the identificator of the leader
 * @author Edoardo Antonini
 *
 */
public class leaderToIP  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String agent = args[0].toString().replace("\"", "");
	   int agentNumber = Integer.parseInt(agent);
	   String ip;
	   switch(agentNumber)
	   {
	   case 1: ip = Strings.FIRST_IP; break;
	   case 2: ip = Strings.SECOND_IP; break;
	   case 3: ip = Strings.THIRD_IP; break;
	   default: ip = Strings.LOCALHOST; break;
	   }
       return un.unifies(args[1], new StringTermImpl(ip));
   }
}
