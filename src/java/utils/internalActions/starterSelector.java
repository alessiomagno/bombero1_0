package utils.internalActions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Select the agents the will start the ring algorithm
 * @author Edoardo Antonini
 *
 */
public class starterSelector  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String[] agentsList = args[0].toString().replace("[", "").replace("]","").split(",");
	   String starter = null;
	   if(agentsList.length <= 1)
	   {
		   JasonException exception = new JasonException("Cannot create ring");
		   throw exception;
	   }else
	   {
		   starter = agentsList[agentsList.length-1];
	   }
       return un.unifies(args[1], new StringTermImpl(starter));
   }
}
