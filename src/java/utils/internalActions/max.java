package utils.internalActions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Find the max number of a list
 * @author Edoardo Antonini
 *
 */
public class max extends DefaultInternalAction{

	private static final long serialVersionUID = 6166402505023621004L;

	@Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception 
	{
	   int max=-1;
	   String[] agentsNumbersList = args[0].toString().replaceAll("[^0-9,]","").split(",");
	   if(agentsNumbersList.length <= 1)
	   {
		   new JasonException("Cannot find max, i'm alone");
		   }else
		   {
			   for(int i=0; i<agentsNumbersList.length; i++)
			   {
				   if(Integer.parseInt(agentsNumbersList[i]) > max)
				   {
					  max = Integer.parseInt(agentsNumbersList[i]);
				   }
			   }
		   }
	       return un.unifies(args[1], new StringTermImpl(Integer.toString(max)));
	   }
}
