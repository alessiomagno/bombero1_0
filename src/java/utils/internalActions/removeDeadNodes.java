package utils.internalActions;

import java.util.ArrayList;
import java.util.Arrays;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.ListTermImpl;
import jason.asSyntax.ObjectTermImpl;
import jason.asSyntax.Term;
/**
 * Removes the dead agent nodes from the agents list
 * @author Edoardo Antonini
 *
 */
public class removeDeadNodes  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   if(args[1] != null)
	   {
		   String[] deadArray = args[1].toString().replace("[", "").replace("]","").split(",");
		   String[] fullArray = args[0].toString().replace("[", "").replace("]","").split(",");
		   ArrayList<String> deadList = new ArrayList<>(Arrays.asList(deadArray));
		   ArrayList<String> fullList = new ArrayList<>(Arrays.asList(fullArray));
		   ArrayList<String> toRemove = new ArrayList<>();
		   for(String s : fullList)
		   {
			   if(deadList.contains(s))
			   {
				   toRemove.add(s);
			   }
		   }
		   fullList.removeAll(toRemove);
		   ListTermImpl result = new ListTermImpl();
		   for(String s : fullList)
		   {
			   result.add(new ObjectTermImpl(s));
		   }

		   return un.unifies(args[2], result);
	   }
	   else
	   {
		   return un.unifies(args[2], args[0]);
	   }
	   
   }
}
