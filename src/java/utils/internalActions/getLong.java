package utils.internalActions;

import com.google.gson.JsonObject;

import env.ForestDataRetriver;
import env.IForestDataRetriver;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.NumberTermImpl;
import jason.asSyntax.Term;
import utils.Strings;
/**
 * Gets the longitude data
 * @author Edoardo Antonini
 *
 */
public class getLong extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;
   private final IForestDataRetriver dataRetriver = ForestDataRetriver.getInstance();

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {

		JsonObject json = this.dataRetriver.getSensorData();
		Double gpsLong = json.getAsJsonObject(Strings.MOCK_GPS_SENSOR).get(Strings.LONG).getAsDouble();
       return un.unifies(args[0], new NumberTermImpl(gpsLong));
   }
}
