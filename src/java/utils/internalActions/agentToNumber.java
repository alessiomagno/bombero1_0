package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Converts the agent name in his number
 * @author Edoardo Antonini
 *
 */
public class agentToNumber extends DefaultInternalAction {

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String agentName = args[0].toString();
	   String agentNumber = args[0].toString().substring(agentName.length()-1);
       return un.unifies(args[1], new StringTermImpl(agentNumber));
   }
}
