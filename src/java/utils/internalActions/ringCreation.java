package utils.internalActions;

import jason.JasonException;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Starts the ring creation algorithm by creating the logical ring
 * @author Edoardo Antonini
 *
 */
public class ringCreation  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String[] agentsList = args[1].toString().replace("[", "").replace("]","").split(",");
	   String next = null;
	   String agent = args[0].toString();
	   if(agentsList.length <= 1)
	   {
		   JasonException exception = new JasonException("Cannot create ring");
		   throw exception;
	   }else
	   {
		   for(int i=0; i<agentsList.length; i++)
		   {
			   if(agentsList[i].equals(agent))
			   {
				  next = agentsList[(i+1)%agentsList.length];
			   }
		   }
	   }
       return un.unifies(args[2], new StringTermImpl(next));
   }
}
