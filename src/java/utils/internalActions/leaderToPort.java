package utils.internalActions;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Derives the TuCSoN port from the identificator of the leader
 * @author Edoardo Antonini
 *
 */
public class leaderToPort  extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String agent = args[0].toString().replace("\"", "");
	   int port = 20500+Integer.parseInt(agent);
       return un.unifies(args[1], new StringTermImpl(String.valueOf(port)));
   }
}
