package utils.internalActions;

import alice.tucson.service.TucsonNodeService;
import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
/**
 * Checks if the TuCSoN node is on
 * @author Edoardo Antonini
 *
 */
public class checkTucsonNode extends DefaultInternalAction{

   private static final long serialVersionUID = 1L;

   @Override
   public Object execute(final TransitionSystem ts, final Unifier un,
           final Term[] args) throws Exception {
	   String result="off";
	   if(TucsonNodeService.isInstalled(args[2].toString().replace("\"", ""),Integer.parseInt(args[1].toString().replace("\"", "")), 2000))
	   {
			result = "on";
	   }
       return un.unifies(args[0], new StringTermImpl(result));
   }
}
