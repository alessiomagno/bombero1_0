package utils.internalActions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiFunction;

import jason.asSemantics.DefaultInternalAction;
import jason.asSemantics.TransitionSystem;
import jason.asSemantics.Unifier;
import jason.asSyntax.StringTermImpl;
import jason.asSyntax.Term;
import utils.MagicNumbers;
import utils.Utils;
import webInterface.BomberoDashboard;
/**
 * Class that calculates the global risk based on all the data
 * @author Edoardo Antonini
 *
 */
public class riskCalculator extends DefaultInternalAction{
	   private static final long serialVersionUID = 1L;

	   @Override
	   public Object execute(final TransitionSystem ts, final Unifier un,
	           final Term[] args) throws Exception {
		   List<String> temps = new ArrayList<String>(Arrays.asList((args[0].toString()
				   										.replace("[", "")
				   										.replace("]","")
				   										.replace("\"", "")
				   										.split(","))));
		   temps.add(args[1].toString());
		   List<String> winds = new ArrayList<String>(Arrays.asList((args[2].toString()
				   										.replace("[", "")
				   										.replace("]","")
				   										.replace("\"", "")
				   										.split(","))));
		   winds.add(args[3].toString());
		   List<String> humidities = new ArrayList<String>(Arrays.asList((args[4].toString()
				   											.replace("[", "")
				   											.replace("]","")
					   										.replace("\"", "")
				   											.split(","))));
		   humidities.add(args[5].toString());
		   System.out.println("Humidities : "+humidities);
		   int tempRisk = tempRiskCalculator(calculateMediumValue(temps));
		   int windRisk = windRiskCalculator(calculateMediumValue(winds));
		   int humRisk = humRiskCalculator(calculateMediumValue(humidities));
		   int totalRisk = totalRiskEvaluation(tempRisk,windRisk,humRisk);
		   String riskString = Utils.riskToString(totalRisk);
		   if(totalRisk != MagicNumbers.NONE)
		   {
			   BomberoDashboard.setWarning(true);
		   }
		   BomberoDashboard.setRisk(totalRisk);
	       return un.unifies(args[6], new StringTermImpl(riskString));
	   }
	   
	   private double calculateMediumValue(List<String> list)
	   {
		   double result=0;
		   for(String t : list)
		   {
			  result += Double.parseDouble(t); 
		   }
		   return result /= list.size();
	   }
	   
	   private int riskValueCalculator(Double value, int elevatedBound, int criticalBound, int extremelyCriticalBound, BiFunction<Double,Integer,Boolean> function)
	   {
		   if(function.apply(value, extremelyCriticalBound))
		   {
			   return MagicNumbers.EXTREMELY_CRITICAL;
		   }else if(function.apply(value, criticalBound))
		   {
			   return MagicNumbers.CRITICAL;
		   }else if(function.apply(value, elevatedBound))
		   {
			   return MagicNumbers.ELEVATED;
		   }else
		   {
			   return MagicNumbers.NONE;
		   }
	   }
	   
	   private int tempRiskCalculator(Double value)
	   {
		   return riskValueCalculator(value, MagicNumbers.ELEVATED_TEMP_BOUND, MagicNumbers.CRITICAL_TEMP_BOUND, MagicNumbers.EXTREMELY_TEMP_BOUND, (a,b) -> a > b);
	   }
	   
	   private int windRiskCalculator(Double value)
	   {
		   return riskValueCalculator(value, MagicNumbers.ELEVATED_WIND_BOUND, MagicNumbers.CRITICAL_WIND_BOUND, MagicNumbers.EXTREMELY_WIND_BOUND, (a,b) -> a > b);
	   }
	   
	   private int humRiskCalculator(Double value)
	   {
		   return riskValueCalculator(value, MagicNumbers.ELEVATED_HUM_BOUND, MagicNumbers.CRITICAL_HUM_BOUND, MagicNumbers.EXTREMELY_HUM_BOUND, (a,b) -> a < b);
	   }
	   
	   private int totalRiskEvaluation(int tempRisk,int windRisk,int humRisk)
	   {
		   int riskSum = tempRisk+windRisk+humRisk;
		   if(riskSum/3 >= MagicNumbers.EXTREMELY_CRITICAL)
		   {
			   return MagicNumbers.EXTREMELY_CRITICAL;
		   }else if(riskSum/3 >= MagicNumbers.CRITICAL)
		   {
			   return MagicNumbers.CRITICAL;
		   }else if(riskSum/3 >= MagicNumbers.ELEVATED)
		   {
			   return MagicNumbers.ELEVATED;
		   }
		   return MagicNumbers.NONE;
	   }
	   
}
