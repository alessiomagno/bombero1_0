package utils;
/**
 * A class representing a pair of values
 * @author Edoardo Antonini
 *
 * @param <T> the first element generic type
 * @param <U> the second element generic type
 */
public class Pair<T, U> {

    private T first;
    private U second;

    public Pair(T first, U second) {
        this.first = first;
        this.second = second;
    }

    public T getFirst() { return first; }
    public U getSecond() { return second; }
    
    
	@Override
	public String toString() {
		return "Pair [first=" + first + ", second=" + second + "]";
	}
    
    
}
