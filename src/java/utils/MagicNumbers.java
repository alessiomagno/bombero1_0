package utils;
/**
 * Utility class for Magic Numbers
 * @author Edoardo Antonini
 *
 */
public class MagicNumbers {
	
	public static final int EXTREMELY_CRITICAL = 3;
	public static final int CRITICAL = 2;
	public static final int ELEVATED = 1;
	public static final int NONE = 0;
	public static final double GAS_THRESOLD = 100;
	public static final int ELEVATED_WIND_BOUND = 15;
	public static final int CRITICAL_WIND_BOUND = 20;
	public static final int EXTREMELY_WIND_BOUND = 30;
	public static final int ELEVATED_TEMP_BOUND = 13;
	public static final int CRITICAL_TEMP_BOUND = 16;
	public static final int EXTREMELY_TEMP_BOUND = 22;
	public static final int ELEVATED_HUM_BOUND = 30;
	public static final int CRITICAL_HUM_BOUND = 25;
	public static final int EXTREMELY_HUM_BOUND = 17;

}
