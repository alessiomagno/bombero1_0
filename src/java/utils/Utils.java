package utils;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
/**
 * A class with general utility functions
 * @author Edoardo Antonini
 *
 */
public class Utils {

	public static double round(double number, int decimals) {
		StringBuilder stb = new StringBuilder();
		Locale.setDefault(Locale.US);
		DecimalFormat format = (DecimalFormat) DecimalFormat.getInstance();
		DecimalFormatSymbols symbols = format.getDecimalFormatSymbols();
		char separator = symbols.getDecimalSeparator();
		stb.append("#" + separator);
		for (int i = 0; i < decimals; i++) {
			stb.append("#");
		}
		DecimalFormat df = new DecimalFormat(stb.toString());
		return Double.parseDouble(df.format(number));

	}
	
	public static String riskToString(int risk)
	{
		String result ="";
		if(risk == MagicNumbers.EXTREMELY_CRITICAL)
		{
			result = "Extremely critical";
		}else if(risk == MagicNumbers.CRITICAL)
		{
			result = "Critical";
		}else if(risk == MagicNumbers.ELEVATED)
		{
			result = "Elevated";
		}else if(risk == MagicNumbers.NONE)
		{
			result = "None";
		}
		return result;
	}
}  
