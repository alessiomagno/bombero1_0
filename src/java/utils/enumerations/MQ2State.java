package utils.enumerations;
/**
 * The enum representing the MQ2 possible states
 * @author Alessio Addimando
 *
 */
public enum MQ2State{
	HIGH,LOW;
	
	
	@Override
	public String toString() {
		return super.name();
	}
}
