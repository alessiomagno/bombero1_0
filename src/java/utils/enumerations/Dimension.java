package utils.enumerations;
/**
 * The enum representing the physical dimensions
 * @author Alessio Addimando
 *
 */
public enum Dimension {
	TEMP,HUM,GASCONC,WIND;
	
	public String toString() {
		return super.name();
	}
}
