package utils.enumerations;
/**
 * The class representing the unit of measures
 * @author Edoardo Antonini
 *
 */
public enum UnitOfMeasure {
	CELSIUS, PERCENTAGE, KMH, PPM, GPS;

	public String toString() {
		return super.toString().toLowerCase();
	}
}
