package utils;
/**
 * Utility class that manages strings
 * @author Edoardo Antonini
 *
 */
public class Strings {

	public static final String MOCK_CO_SENSOR = "Mock_CO_Sensor";
	public static final String MOCK_TEMPERATURE_SENSOR = "Mock_Temp_Sensor";
	public static final String MOCK_HUMIDITY_SENSOR = "Mock_Humidity_Sensor";
	public static final String MOCK_WIND_SENSOR = "Mock_Wind_Sensor";
	public static final String MOCK_GPS_SENSOR = "Mock_GPS_Sensor";
	public static final String DHT11_SENSOR = "DHT 11 Sensor";
	public static final String MQ2_SENSOR = "MQ2 Sensor";
	public static final String UNIT = "unit of measure";
	public static final String UNIT_1 = "unit of measure1";
	public static final String UNIT_2 = "unit of measure2";
	public static final String VALUE = "value";
	public static final String VALUE_1 = "value1";
	public static final String VALUE_2 = "value2";
	public static final String LOCAL_PREFIX = "l";
	public static final String CENTER_PREFIX = "c";
	public static final String LAT = "lat";
	public static final String LONG = "long";
	public static final String LOCALHOST = "127.0.0.1";
	public static final String FIRST_IP = "192.168.1.64";
	public static final String SECOND_IP = "192.168.1.69";
	public static final String THIRD_IP = "192.168.1.110";

}
