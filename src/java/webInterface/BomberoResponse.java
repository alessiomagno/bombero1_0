package webInterface;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import utils.enumerations.web.ContentType;
import utils.enumerations.web.Status;
/**
 * 
 * @author Alessio Addimando
 * Custom response to a http-GET request with the Bombero dashboard upgraded.
 */

public class BomberoResponse {


	public static final String VERSION = "HTTP/1.0";

	private List<String> headers = new ArrayList<String>();

	private byte[] body;
	
	private BomberoDashboard dashboard;

	public BomberoResponse(BomberoDashboardRequest req) throws IOException {

		switch (req.method) {
			case HEAD:
				fillHeaders(Status._200);
				break;
			case GET:
				try {
					fillHeaders(Status._200);
					this.dashboard = new BomberoDashboard();
					headers.add(ContentType.HTML.toString());
					fillResponse(this.dashboard.getDashboard());
					
				} catch (Exception e) {
					System.out.println("Response Error"+ e);
					fillHeaders(Status._400);
					fillResponse(Status._400.toString());
				}

				break;
			case UNRECOGNIZED:
				fillHeaders(Status._400);
				fillResponse(Status._400.toString());
				break;
			default:
				fillHeaders(Status._501);
				fillResponse(Status._501.toString());
		}

	}

	private void fillHeaders(Status status) {
		headers.add(BomberoResponse.VERSION + " " + status.toString());
		headers.add("Connection: close");
		headers.add("Server: BomberoSupervisor");
	}

	private void fillResponse(String response) {
		body = response.getBytes();
	}

	public void write(OutputStream os) throws IOException {
		DataOutputStream output = new DataOutputStream(os);
		for (String header : headers) {
			output.writeBytes(header + "\r\n");
		}
		output.writeBytes("\r\n");
		if (body != null) {
			output.write(body);
		}
		output.writeBytes("\r\n");
		output.flush();
	}

}
