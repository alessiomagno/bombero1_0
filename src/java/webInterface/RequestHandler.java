package webInterface;

import java.net.Socket;
/**
 * 
 * @author Alessio Addimando
 * Generic handler to manage requests
 */
public class RequestHandler implements Runnable {


	private Socket socket;

	public RequestHandler(Socket socket) {
		this.socket = socket;
	}

	public void run() {
		try {
			BomberoDashboardRequest req = new BomberoDashboardRequest(socket.getInputStream());
			BomberoResponse res = new BomberoResponse(req);
			res.write(socket.getOutputStream());
			socket.close();
		} catch (Exception e) {
			System.out.println("Runtime Error" +e);
		}
	}
}
