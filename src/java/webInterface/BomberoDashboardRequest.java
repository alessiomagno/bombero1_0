package webInterface;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import utils.enumerations.web.Method;
/**
 * 
 * @author Alessio Addimando
 * Custom request of the Bombero dashboard upgraded.
 */

public class BomberoDashboardRequest {


	List<String> headers = new ArrayList<String>();

	Method method;

	public BomberoDashboardRequest(InputStream is) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String str = reader.readLine();
		parseRequestLine(str);

		while (!str.equals("")) {
			str = reader.readLine();
			parseRequestHeader(str);
		}
	}

	private void parseRequestLine(String str) {
		System.out.println("Request of the dashboard...");
		String[] split = str.split("\\s+");
		try {
			method = Method.valueOf(split[0]);
		} catch (Exception e) {
			method = Method.UNRECOGNIZED;
		};
	}

	private void parseRequestHeader(String str) {
		headers.add(str);
	}
}
