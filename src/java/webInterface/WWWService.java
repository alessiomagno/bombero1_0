package webInterface;

import java.io.IOException;
import java.net.ServerSocket;
/**
 * 
 * @author Alessio Addimando
 * WebService that starts on a listening indipendent thread.
 */

public class WWWService extends Thread {


	private static final int DEFAULT_PORT = 8080;
	private boolean webMonitor = true;

	public void start(int port) throws IOException {
		ServerSocket s = new ServerSocket(port);
		System.out.println("Bombero WebService listening on port " + port);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while (webMonitor) {
					try {
						new Thread(new RequestHandler(s.accept())).start();
					} catch (IOException e) {
						e.printStackTrace();
					};
				}
			}
		}).start();
		s.close();
	}
	/**
	 * Possibility to map the default port
	 * @return
	 * @throws NumberFormatException
	 */

	public static int getValidPortParam() throws NumberFormatException {
		
		return DEFAULT_PORT;
	}
	
	public synchronized void stopService() {
		this.webMonitor = false;
	}
	
	/**
	 * Main to start&test WebService
	 * @param args
	 */
	public static void main(String args[]) {
		try {
			new WWWService().start(WWWService.getValidPortParam());
		} catch (Exception e) {
			System.out.println("Startup Error"+ e);
			
		}
	}
}
