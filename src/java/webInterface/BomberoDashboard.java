package webInterface;

import utils.MagicNumbers;
import utils.Utils;
/**
 * 
 * @author Alessio Addimando
 * Dashboard for a real-time monitoring of the general situation of an area.
 *
 */
public class BomberoDashboard {
	
	private static BomberoDashboard instance = null;
	private static boolean fireAlarm = false;
	private static boolean fireWarning = false;
	private static String GPSCoordinates ="";
	private static int risk = MagicNumbers.NONE;
	

	public synchronized static BomberoDashboard getInstance()
	    {
	      if (instance == null)
	      {
	    	  instance = new BomberoDashboard();
	      }

	      return instance; 
	    }
	
	public static void setAlarm(boolean falarm){
		fireAlarm = falarm;
		
	}
	public static void setWarning(boolean fwarning){
		fireWarning = fwarning;
	}
	public static void setGPSCoordinate(String GPSC){
		GPSCoordinates = GPSC;	
	}
	public static void setRisk(int frisk){
		risk = frisk;
	}
	/**
	 * 
	 * @return the HTML page to show the updated dashboard 
	 */
	public synchronized String getDashboard() {
		
		StringBuilder dashboard = new StringBuilder("<html><head><title>Bombero Dashboard</title></head><body style=\"background-color:#ff4d4d;\">");
		dashboard.append("<h1>Bombero Dashboard</h1><br>");
		if(fireAlarm)
		{
			dashboard.append("<h3>It seems that in the area "+GPSCoordinates+" there's a fire.</h3>");
		}
		if(fireWarning)
		{
			dashboard.append("<h3>The risk of fire is ");
			dashboard.append(Utils.riskToString(risk));
			dashboard.append("</h3>");
		}
		if(!fireAlarm & !fireWarning) 
		{
			dashboard.append("<h3>No problems!</h3>");
		}
		dashboard.append("</body></html>");
		return dashboard.toString();
		
	}
	
	
	

}
