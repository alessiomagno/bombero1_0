package env;

import com.google.gson.JsonObject;
/**
 * Interface with the function to retrieve sensors' data
 * @author Edoardo Antonini
 *
 */
public interface IForestDataRetriver {

	/**
	 * Function that gets the sensors' data
	 * @return a JsonObject containing the data
	 */
	JsonObject getSensorData();
}
