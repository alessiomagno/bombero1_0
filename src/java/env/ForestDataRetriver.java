package env;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.JsonObject;

import sensors.DHT11Sensor;
import sensors.ICOSensor;
import sensors.IGPSSensor;
import sensors.IHumiditySensor;
import sensors.ISensor;
import sensors.ITemperatureSensor;
import sensors.IWindSensor;
import sensors.MQ2Sensor;
import sensors.MockGpsSensor;
import sensors.MockWindSensor;
import utils.Strings;
import utils.enumerations.UnitOfMeasure;
/**
 * Class that defines the sensors and the retrieves the data @see{IForestDataRetriever}
 * @author Edoardo Antonini
 *
 */
public class ForestDataRetriver implements IForestDataRetriver {

	private static IForestDataRetriver instance;
	private List<ISensor> sensors;
	private IWindSensor windSens = new MockWindSensor();
	private IGPSSensor gpsSensor = new MockGpsSensor();
	private DHT11Sensor dht11 = new DHT11Sensor();
	private MQ2Sensor mq2 = new MQ2Sensor();

	private ForestDataRetriver() {
		sensors = new ArrayList<>();
		sensors.add(dht11);
		sensors.add(mq2);
		sensors.add(windSens);
		sensors.add(gpsSensor);
	}

	public static IForestDataRetriver getInstance() {
		if (instance == null) {
			instance = new ForestDataRetriver();
		}
		return instance;
	}

	@Override
	public JsonObject getSensorData() {

		JsonObject json = new JsonObject();
		for (ISensor s : this.sensors) {
			JsonObject sensor = new JsonObject();
			if(s instanceof IGPSSensor)
			{
				sensor.addProperty(Strings.LAT, ((IGPSSensor) s).getLat());
				sensor.addProperty(Strings.LONG, ((IGPSSensor) s).getLong());
			}
			else if(s instanceof DHT11Sensor)
			{
				sensor.addProperty(Strings.VALUE_1, ((DHT11Sensor) s).getTemp());
				sensor.addProperty(Strings.VALUE_2, ((DHT11Sensor) s).getHumidity());
				sensor.addProperty(Strings.UNIT_1, UnitOfMeasure.CELSIUS.toString());
				sensor.addProperty(Strings.UNIT_2, UnitOfMeasure.PERCENTAGE.toString());
			}
			else if(s instanceof ITemperatureSensor)
			{
				sensor.addProperty(Strings.VALUE, ((ITemperatureSensor) s).getTemp());
				sensor.addProperty(Strings.UNIT, UnitOfMeasure.CELSIUS.toString());
			}
			else if(s instanceof IHumiditySensor)
			{
				sensor.addProperty(Strings.VALUE, ((IHumiditySensor) s).getHumidity());
				sensor.addProperty(Strings.UNIT, UnitOfMeasure.PERCENTAGE.toString());
			}
			else if(s instanceof IWindSensor)
			{
				sensor.addProperty(Strings.VALUE, ((IWindSensor) s).getWindSpeed());
				sensor.addProperty(Strings.UNIT, UnitOfMeasure.KMH.toString());
			}
			else if(s instanceof ICOSensor)
			{
				sensor.addProperty(Strings.VALUE, ((ICOSensor) s).getConcentration());
				sensor.addProperty(Strings.UNIT, UnitOfMeasure.PPM.toString());
			}
			json.add(s.getName(), sensor);
		}
		return json;
	}

}
