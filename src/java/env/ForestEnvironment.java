package env;
import jason.asSyntax.*;
import jason.environment.*;
/**
 * Environment for Jason MAS, currently useless because of a bug in Jason
 * @author Edoardo Antonini
 *
 */
public class ForestEnvironment extends Environment {

	private static ForestEnvironment environment;


	public  ForestEnvironment() {
		environment = this;
	}
	/** Called before the MAS execution with the args informed in .mas2j */
	@Override
	public void init(String[] args) {
		super.init(args);
		environment = this;
		// addPercept(ASSyntax.parseLiteral("percept(demo)"));
		//this.tucsonNode = InstanceTucsonNode.getIstance();
	}

	@Override
	public boolean executeAction(String agName, Structure action) {

		return true;
	}
	
	public static ForestEnvironment getInstance()
	{
		return environment;
	}


	/** Called before the end of MAS execution */
	@Override
	public void stop() {
		super.stop();
	}
}
