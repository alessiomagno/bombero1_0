// Agent bomberino in project bombero
deadNodes(DeadList).
/* Initial beliefs and rules */
// Could add the tuple centre as a configurable parameter 
/* Initial goals */
//!startTucsonNode.
!ringCreation.
//!removeOldData.
//!writeData.

/* Plans */
+!ringCreation : .findall(X,deadNodes(X),DeadList) <- .wait(2000);
														.abolish(node(_));
														.abolish(list(_));
														.abolish(leader(_,_));
														.abolish(next(_));
														.abolish(initiator(_));
														.my_name(Me);
														.all_names(List);
														utils.internalActions.removeDeadNodes(List,DeadList,NewList);
														.print(NewList);
														utils.internalActions.ringCreation(Me,NewList,Next);
														-+next(Next);
														utils.internalActions.starterSelector(NewList,Starter);
														-+starter(Starter);
														.print(Starter);
														!leaderElection.
+!leaderElection : next(Next) &
					 starter(Starter)<-.abolish(node(_));
										.abolish(list(_));
										.abolish(leader(_,_));
										.abolish(initiator(_));
										.my_name(Me);
										.term2string(Me,AgName);
										if(AgName == Starter)
										{
											utils.internalActions.agentToNumber(Me,AgNumber);
											.send(Next,tell,list([AgNumber]));
											.wait(50);
											.send(Next,tell,initiator(Me));
										}.
+!listDiffusion : list(List) & 
					next(Next) &
					initiator(Initiator) <-.my_name(Me);
											if(Me == Initiator)
											{
												.print(List);
												utils.internalActions.max(List,Max);	
												.send(Next,tell,leader(Max,Me));
												.print("Leader: ",Max);
											}else
											{
												utils.internalActions.agentToNumber(Me,AgNumber);
												.concat(List,[AgNumber],Result);
												.send(Next,tell,list(Result));
												.wait(50);
												.send(Next,tell,initiator(Initiator));
											}.

+!emergencyDiffusion : list(List) &
						help(AgName) &
						initiator(Initiator) <- utils.internalActions.agentToNumber(Me,AgNumber);
												.concat(List,[AgNumber],Result);
												.send(AgName,tell,list(Result));
												.wait(50);
												.send(AgName,tell,initiator(Initiator))
												.wait(50);
												.send(AgName,tell,helped(ok)).

+!leaderDiffusion : leader(Leader,LeaderElector) & next(Next) <- .my_name(Me);
																if(not(LeaderElector == Me))
																{	
																	.send(Next,tell,leader(Leader,LeaderElector));
																}.														
+!startWebService : true <- utils.internalActions.startWebService(Me).

+!startTucsonNode: leader(AgNumber,_) <- utils.internalActions.leaderToPort(AgNumber,Port);
										utils.internalActions.startTucsonNode(Node,Port);
										.wait(4000); 
										!checkTucsonNode.

+!checkTucsonNode: leader(AgNumber,_) <- utils.internalActions.leaderToPort(AgNumber,Port);
										utils.internalActions.leaderToIP(AgNumber,IP);
										utils.internalActions.checkTucsonNode(Result,Port,IP);
										-+node(Result).

+!deleteOldData:  node("on") &
				 leader(AgNumber,_)<- .wait(1000);
										.date(AN,ME,GI);
										utils.internalActions.leaderToPort(AgNumber,Port);
										utils.internalActions.leaderToIP(AgNumber,IP);
										tucson4jason.api.rdAll("default", IP, Port,data(_,temp(_),co(_),wind(_),humidity(_),gps(_,_),_,_,_),Rd2); 
										tucson4jason.api.getResult(Rd2,Res2);
										if(.list(Res2) & (not .empty(Res2))){
												for(.member (S,Res2)){
													tucson4jason.api.getArg(S,6,DataANString);
													tucson4jason.api.getArg(S,7,DataMEString);
													tucson4jason.api.getArg(S,8,DataGIString);
													utils.internalActions.string2int(DataGIString,DataGI);
													utils.internalActions.string2int(DataMEString,DataME);
													utils.internalActions.string2int(DataANString,DataAN);
													if(not(DataAN == AN & DataME == ME & DataGI == GI))
													{
														tucson4jason.api.getArg(S,0,ID);
														tucson4jason.api.inp("default",IP,Port,data(_,temp(_),co(_),wind(_),humidity(_),gps(_,_),DataAN,DataME,DataGI),In1);
													}
												}
											
										}
										.wait(86399000);//waits a day
										!deleteOldData.
												
+!retriveSensorData : true <- 
						.wait(1000);
						utils.internalActions.getTemp(Temp);
						utils.internalActions.getWind(Wind);
						utils.internalActions.getHumidity(Humidity);
						utils.internalActions.getCo(Co);
						utils.internalActions.getLat(GpsLat);
						utils.internalActions.getLong(GpsLong);
						-+ltemp(Temp);
						-+lco(Co);
						-+lhumidity(Humidity);
						-+lwind(Wind);
						-+lgps(GpsLat,GpsLong);
						!retriveSensorData.
						
+!retriveCenterData : leader(AgNumber,_) <-  .abolish(emergency);//to delete old belifs
											.abolish(cco(_));
											.abolish(ctemp(_));
											.abolish(cwind(_));
											.abolish(chumidity(_));
											utils.internalActions.leaderToPort(AgNumber,Port);
											utils.internalActions.leaderToIP(AgNumber,IP);
											tucson4jason.api.rdAll("default",IP,Port,data(_,temp(_),co(_),wind(_),humidity(_),gps(_,_),_,_,_),Rd1);
											tucson4jason.api.getResult(Rd1,Res1);
											if(.list(Res1) & (not .empty(Res1))){
												for(.member (Tuple,Res1)){
													tucson4jason.api.getArg(Tuple,0,DataPublisher);
													.my_name(Me);
													.term2string(Me,AgName);
													if(not (AgName == DataPublisher)){
														tucson4jason.api.getArg(Tuple,1,Temp);
														tucson4jason.api.getArg(Tuple,2,Co);
														tucson4jason.api.getArg(Tuple,3,Wind);
														tucson4jason.api.getArg(Tuple,4,Humidity);
														tucson4jason.api.getArg(Tuple,5,Gps);
														tucson4jason.api.getArg(Temp,0,F);
														tucson4jason.api.getArg(Co,0,I);
														tucson4jason.api.getArg(Wind,0,R);
														tucson4jason.api.getArg(Humidity,0,E);
														tucson4jason.api.getArg(Gps,0,G);
														tucson4jason.api.getArg(Gps,1,A);
														+ctemp(F);
														+cco(I,G,A);
														+cwind(R);
														+chumidity(E);
													}
												}
												
											}
											!!fireChecker
											!!computeGlobalRisk;
											.wait(3000);
											!retriveCenterData.
								
+!fireChecker : lco(LocalCo) 
				& lgps(Lat,Long)
				& .findall(c(A,B,C),cco(A,B,C),CenterCos) <- utils.internalActions.triangulate(LocalCo,Lat,Long,CenterCos,ResultLat,ResultLong,Emergency);
															if(Emergency >= 1) 
															{
																-+emergency
																.println("Help it's all burning in ",ResultLat, ",",ResultLong);
															}
															else
															{
																.println("No fire detected");
															}.


//to adjust compute risk for the tuple center
 +!computeGlobalRisk : not emergency &
 						.findall(A, ctemp(A),CenterTemps) & 
 						.findall(C, cwind(C),CenterWinds) & 
 						.findall(D, chumidity(D), CenterHums) &
 						ltemp(LocalTemp) &
 						lwind(LocalWind) &
 						lhumidity(LocalHum) <- if((not .empty(CenterTemps)) & (not .empty(CenterWinds)) & (not .empty(CenterHums)))
						 						{
							 						utils.internalActions.riskCalculator(CenterTemps,LocalTemp,CenterWinds,LocalWind,CenterHums,LocalHum,TotalRisk);
							 						-+risk(TotalRisk);
													.print("risk is: ",TotalRisk);
												}.

								
+!writeData : ltemp(Temp) &
				lco(Co) &
				lwind(Wind) &
				lhumidity(Hum) &
				lgps(Lat,Long) &
				leader(AgNumber,_) <- 	.wait(1000);
										utils.internalActions.leaderToPort(AgNumber,Port);
										utils.internalActions.leaderToIP(AgNumber,IP);
										.my_name(Me);
										.print("Writing to tuple center:");
										.print(" Temp ", Temp);
										.print(" Co ", Co);
										.print(" Wind ", Wind);
										.print(" Humidity ", Hum);
										.print(" GPS ",Lat, " ",Long)
										.date(AN,ME,GI);
										tucson4jason.api.out("default", IP, Port,data(Me,temp(Temp),co(Co),wind(Wind),humidity(Hum),gps(Lat,Long),AN,ME,GI), Out1); //test t4jn
										!writeData.
						


+!pingLeader : leader(Leader,_) & (not (Leader == "0")) <- .wait(1000);
														.concat("bomberino",Leader,LeaderName);
														.send(LeaderName,inform,ping);
														!pingLeader.
									
//se il leader muore									
+!kqml_received(ams, failure,Message,_) : true <- .print(Message);.drop_all_intentions;
												utils.internalActions.findDeadAgent(Message,Agent);
												+deadNodes([Agent]);
												.abolish(node(_));
												.abolish(list(_));
												.abolish(leader(_,_));
												.abolish(next(_));
												.abolish(initiator(_));
												!ringCreation.

//failure
-!startTucsonNode : true <- .print("Tucson is already on");
							!!checkTucsonNode.

-!writeData : true <- if(not node("on"))
						{
							.print("Tucson is not yet on");	
						}else
						{
							.print("Data not available");
						}
						.wait(500)
						!writeData.

-!fireChecker : true <- true.

-!checkTucsonNode : true <- .print("Node is on").

-!retriveCenterData : true <- .print("waiting Tucson to activate");
								.wait(500);
								!retriveCenterData.
-!retriveSensorData[error(ia_failed)] : true <- .wait(200);!retriveSensorData.

//se un nodo cade durante la scelta del leader dell'anello ricreo l'anello

-!leaderElection[code(.send(X,tell,Y))] : true <- !ringCreation.

-!listDiffusion[code(.send(X,tell,Y))] : true <- !ringCreation;
												!listDiffusion.											
-!listDiffusion[error(no_applicable)] : initiator(Initiator) &  next(Next)<- .print("list not arrived");
																			.wait(500);
																			.my_name(Name);
																			.send(Next,tell,help(Name));
																			!listDiffusion.
-!listDiffusion[error(no_applicable)] : next(Next) & list(List) <- .print("initiator not arrived");
																	.wait(500);
																	.my_name(Name);
																	.send(Next,tell,help(Name));
																	!listDiffusion.

-!listDiffusion[error(no_applicable)] : next(Next) <- .print("initiator & list not arrived");
														.wait(500);
														.my_name(Name);
														.send(Next,tell,help(Name));
														!listDiffusion.
												
-!listDiffusion[error(no_applicable)] : true <- .print("just wait a second");
												.wait(500);
												!listDiffusion.	
												
-!leaderDiffusion[code(.send(X,tell,Y))] : true <- !ringCreation;
												!listDiffusion.

-!emergencyDiffusion : next(Next) & starter(Starter) & help(Name) <-.my_name(Me);
									.term2string(Me,AgName);
									if(AgName == Starter)
									{
										utils.internalActions.agentToNumber(Me,AgNumber);
										.send(Name,tell,list([AgNumber]));
										.wait(50);
										.send(Name,tell,initiator(Me));
										.wait(50);
										.send(Name,tell,helped(ok));
									}else
									{
										.send(Next,tell,help(Me));
									}.												
//se ho solo un agente
-!ringCreation[error_msg("Cannot create ring")] : true <- -+leader("0","bomberino0").

/*Perception */

+leader(Leader,LeaderElector) : Leader == "0" <- !!startTucsonNode.

+leader(Leader,LeaderElector) : .my_name(Me) &
								utils.internalActions.agentToNumber(Me,AgNumber) & 
								leader(AgNumber,_) <- !leaderDiffusion;
														!!startTucsonNode.

+leader(Leader,LeaderElector) : .my_name(Me) & 
								utils.internalActions.agentToNumber(Me,AgNumber) & 
								not leader(AgNumber,_) <- !leaderDiffusion;
															!!checkTucsonNode.	

+initiator(Initiator) : true <- !listDiffusion. 

+node("on") : true <- !!retriveSensorData;
						!!writeData;
						if(.my_name(Me) & 
							utils.internalActions.agentToNumber(Me,AgNumber) & 
							leader(Leader,_) &
							(Leader == "0" | Leader == AgNumber))
						{
							!!deleteOldData;
							!!retriveCenterData;
							!!startWebService;
						}
						!!pingLeader.
+node("off") : true <- .wait(500);!checkTucsonNode.

+help(Name) : true <- .print(Name);!!emergencyDiffusion.

+helped(ok) : help(Name) <- .print("Helped");!!emergencyDiffusion.
